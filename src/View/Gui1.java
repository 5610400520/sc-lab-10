package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Gui1 {
	private JFrame frame;
	private JPanel colorPanel;
	private JPanel colorPanelMain;
	private JButton red;
	private JButton green;
	private JButton blue;
	
	public Gui1(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new BorderLayout());
		colorPanel.setBackground(new Color(255,255,255));
		
		colorPanelMain = new JPanel();
		colorPanelMain.setLayout(new FlowLayout());
		colorPanelMain.setBackground(new Color(255,255,255));
		
		red = new JButton("Red");
		red.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				colorPanel.setBackground(Color.red);	
				colorPanelMain.setBackground(Color.red);	
			}
		});
		
		green = new JButton("Green");
		green.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				colorPanel.setBackground(Color.green);	
				colorPanelMain.setBackground(Color.green);
			}
		});
		
		blue = new JButton("Blue");
		blue.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				colorPanel.setBackground(Color.blue);	
				colorPanelMain.setBackground(Color.blue);
			}
		});
		
		colorPanelMain.add(red);
		colorPanelMain.add(green);
		colorPanelMain.add(blue);
		colorPanel.add(colorPanelMain, BorderLayout.SOUTH);
		frame.add(colorPanel, BorderLayout.CENTER);	
		
		frame.setVisible(true);
	}
}
