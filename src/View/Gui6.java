package View;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Control.Controller;

public class Gui6 {
	private int key;
	private Controller control;
	private JFrame frame;
	private JPanel panelMain;
	private JPanel panel;
	private JMenuBar menuBar;
	private JMenu option;
	private JMenuItem deposit;
	private JMenuItem withdraw;
	private JLabel title;
	private JTextField money;
	private JButton submit;
	private JLabel balance;
		
	public Gui6(){
		control = new Controller();
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(530, 140);
		
		panelMain = new JPanel();
		panelMain.setLayout(new FlowLayout());
		
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		
		menuBar = new JMenuBar();
		
		option = new JMenu("Option");
		deposit = new JMenuItem("Deposit");
		withdraw = new JMenuItem("Withdraw");
	
		option.add(deposit);
		option.add(withdraw);
		
		deposit.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				key = 0;
				title.setText(" จำนวนเงินฝาก : ");
			}
		});
		
		withdraw.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				key = 1;
				title.setText(" จำนวนเงินถอน : ");
			}
		});
		
		title = new JLabel(" ขำนวนเงิน : ");		
		
		money = new JTextField(9);
		
		submit = new JButton("submit");
		
		submit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (key == 0){
					control.deposit(money.getText());
					balance.setText("ยอดเงินคงเหลือ:  " + control.getBalance() + "   บาท\n");
				}
				else{
					control.withdraw(money.getText());
					balance.setText("ยอดเงินคงเหลือ:  " + control.getBalance() + "   บาท\n");
				}
			}
		});
		
		
		balance = new JLabel("ยอดเงินคงเหลือ:  0.0    บาท");
		
		menuBar.add(option);
		panel.add(menuBar);
		panel.add(title);
		panel.add(money);
		panel.add(submit);
		panel.add(balance);
		panelMain.add(panel);
		frame.add(panelMain);
		frame.setJMenuBar(menuBar);
		frame.setVisible(true);
	}
}
