package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Gui3 {
	private JFrame frame;
	private JPanel colorPanel;
	private JPanel colorPanelMain;
	private JCheckBox red;
	private JCheckBox green;
	private JCheckBox blue;
	
	class ListenerMgr implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (red.isSelected()){
				colorPanel.setBackground(Color.red);	
				colorPanelMain.setBackground(Color.red);
				if (green.isSelected()){
					colorPanel.setBackground(Color.yellow);	
					colorPanelMain.setBackground(Color.yellow);
					if (blue.isSelected()){
						colorPanel.setBackground(new Color(62,15,247));	
						colorPanelMain.setBackground(new Color(62,15,247));
					}
				}
				else if (blue.isSelected()){
					colorPanel.setBackground(new Color(224,15,247));	
					colorPanelMain.setBackground(new Color(224,15,247));
					if (green.isSelected()){
						colorPanel.setBackground(new Color(62,15,247));	
						colorPanelMain.setBackground(new Color(62,15,247));
					}
				}				
			}
			else if (green.isSelected()){
				colorPanel.setBackground(Color.green);	
				colorPanelMain.setBackground(Color.green);
				if (blue.isSelected()){
					colorPanel.setBackground(new Color(15,247,174));	
					colorPanelMain.setBackground(new Color(15,247,174));
					if (red.isSelected()){
						colorPanel.setBackground(new Color(62,15,247));	
						colorPanelMain.setBackground(new Color(62,15,247));
					}
				}
				else if (red.isSelected()){
					colorPanel.setBackground(new Color(240,247,15));	
					colorPanelMain.setBackground(new Color(240,247,15));
					if (blue.isSelected()){
						colorPanel.setBackground(new Color(62,15,247));	
						colorPanelMain.setBackground(new Color(62,15,247));
					}
				}
			}
			else if (blue.isSelected()){
				colorPanel.setBackground(Color.blue);	
				colorPanelMain.setBackground(Color.blue);
				if (green.isSelected()){
					colorPanel.setBackground(new Color(15,247,174));	
					colorPanelMain.setBackground(new Color(15,247,174));
					if (red.isSelected()){
						colorPanel.setBackground(new Color(62,15,247));	
						colorPanelMain.setBackground(new Color(62,15,247));
					}
				}
				else if (red.isSelected()){
					colorPanel.setBackground(new Color(224,15,247));	
					colorPanelMain.setBackground(new Color(224,15,247));
					if (green.isSelected()){
						colorPanel.setBackground(new Color(62,15,247));	
						colorPanelMain.setBackground(new Color(62,15,247));
					}
				}
			}
			else if (red.isSelected() && green.isSelected() && blue.isSelected()){
				colorPanel.setBackground(new Color(0,0,0));	
				colorPanelMain.setBackground(new Color(0,0,0));
			}
		}
	}
	
	
	public Gui3(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new BorderLayout());
		colorPanel.setBackground(new Color(255,255,255));
		
		colorPanelMain = new JPanel();
		colorPanelMain.setLayout(new FlowLayout());
		colorPanelMain.setBackground(new Color(255,255,255));
		
		red = new JCheckBox("Red");
		red.addActionListener(new ListenerMgr());
		
		green = new JCheckBox("Green");
		green.addActionListener(new ListenerMgr());	
		
		blue = new JCheckBox("Blue");
		blue.addActionListener(new ListenerMgr());
		
		colorPanelMain.add(red);
		colorPanelMain.add(green);
		colorPanelMain.add(blue);
		
		colorPanel.add(colorPanelMain, BorderLayout.SOUTH);
		frame.add(colorPanel, BorderLayout.CENTER);	
		
		frame.setVisible(true);
	}
}
