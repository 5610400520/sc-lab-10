package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Gui2 {
	private JFrame frame;
	private JPanel colorPanel;
	private JPanel colorPanelMain;
	private JRadioButton red;
	private JRadioButton green;
	private JRadioButton blue;
	
	class ListenerMgr implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (red.isSelected()){
				red.setSelected(true);
				blue.setSelected(false);
				green.setSelected(false);
				colorPanel.setBackground(Color.red);	
				colorPanelMain.setBackground(Color.red);
			}
			else if (green.isSelected()){
				green.setSelected(true);
				blue.setSelected(false);
				red.setSelected(false);
				colorPanel.setBackground(Color.green);	
				colorPanelMain.setBackground(Color.green);
			}
			else if (blue.isSelected()){
				blue.setSelected(true);
				red.setSelected(false);
				green.setSelected(false);
				colorPanel.setBackground(Color.blue);	
				colorPanelMain.setBackground(Color.blue);
			}
		}
	}
	
	
	public Gui2(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new BorderLayout());
		colorPanel.setBackground(new Color(255,255,255));
		
		colorPanelMain = new JPanel();
		colorPanelMain.setLayout(new FlowLayout());
		colorPanelMain.setBackground(new Color(255,255,255));
		
		red = new JRadioButton("Red");
		red.addActionListener(new ListenerMgr());
		
		green = new JRadioButton("Green");
		green.addActionListener(new ListenerMgr());	
		
		blue = new JRadioButton("Blue");
		blue.addActionListener(new ListenerMgr());
		
		colorPanelMain.add(red);
		colorPanelMain.add(green);
		colorPanelMain.add(blue);
		
		ButtonGroup bt = new ButtonGroup();		
		bt.add(red);
		bt.add(blue);
		bt.add(green);
		
		colorPanel.add(colorPanelMain, BorderLayout.SOUTH);
		frame.add(colorPanel, BorderLayout.CENTER);	
		
		frame.setVisible(true);
	}
}
