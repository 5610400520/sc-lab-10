package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Gui4 {
	private JFrame frame;
	private JPanel colorPanel;
	private JPanel colorPanelMain;
	private JComboBox<String> List;
	
	class ListenerMgr implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (List.getSelectedItem()=="Red"){
				colorPanel.setBackground(Color.red);	
				colorPanelMain.setBackground(Color.red);
			}
			else if (List.getSelectedItem()=="Green"){
				colorPanel.setBackground(Color.green);	
				colorPanelMain.setBackground(Color.green);
			}
			else if (List.getSelectedItem()=="Blue"){
				colorPanel.setBackground(Color.blue);	
				colorPanelMain.setBackground(Color.blue);
			}
		}
	}
		
	public Gui4(){
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		
		colorPanel = new JPanel();
		colorPanel.setLayout(new BorderLayout());
		colorPanel.setBackground(new Color(255,255,255));
		
		colorPanelMain = new JPanel();
		colorPanelMain.setLayout(new FlowLayout());
		colorPanelMain.setBackground(new Color(255,255,255));
		
		List = new JComboBox<String>();
		List.addItem("Red");
		List.addItem("Green");
		List.addItem("Blue");	
		
		List.addActionListener(new ListenerMgr());
		
		colorPanelMain.add(List);
		colorPanel.add(colorPanelMain, BorderLayout.SOUTH);
		frame.add(colorPanel, BorderLayout.CENTER);	
		
		frame.setVisible(true);
	}
}
