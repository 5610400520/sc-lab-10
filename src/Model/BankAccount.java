package Model;

public class BankAccount {
	private double balance;
	
	public void deposit(String amount){
		double a = Double.parseDouble(amount);
		balance += a;
	}
	
	public void withdraw(String amount){
		double a = Double.parseDouble(amount);
		balance -= a;
	}
	
	public String getBalance(){
		return balance+"";
	}
}
