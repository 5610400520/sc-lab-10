package Control;

import Model.BankAccount;

public class Controller {
	private BankAccount bank;
	private int notWithdraw;
	
	public Controller(){
		bank = new BankAccount(); 
	}
	
	public void deposit(String p){
		bank.deposit(p);
	}
	
	public void withdraw(String p){
		double input = Double.parseDouble(p);
		double balance = Double.parseDouble(bank.getBalance());

		if (balance <input){
			notWithdraw = 1;
		}
		else{
			notWithdraw = 0;
			bank.withdraw(p);
		}
	}
	
	public String getBalance(){
		return bank.getBalance();
	}
	
	public int getValue(){
		return notWithdraw;
	}
}
